<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/admin_panel', 'Dashboard\DashboardController@index')->name('admin_panel.index');
Route::match(['get', 'post'], '/admin_panel/login', 'Dashboard\DashboardController@login')->name('admin_panel.login');

//Dashboard
Route::group(['middleware' => ['admin'], 'prefix' => 'admin_panel', 'namespace' => 'Dashboard'], function () {

    Route::get('/dashboard', 'DashboardController@dashboard')->name('dashboard');

    Route::post('/logout', 'DashboardController@logout')->name('admin_panel.logout');
    //Users
    Route::get('/users', 'UserController@index')->name('user.index');

    Route::post('/users-edit', 'UserController@edit')->name('user.edit');

    Route::post('/users-save', 'UserController@store')->name('user.save');

    Route::post('/users-update', 'UserController@update')->name('user.update');

    Route::post('/users-delete', 'UserController@destroy')->name('user.delete');
});