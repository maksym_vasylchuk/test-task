<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin_role = Role::where('name', 'admin')->first();

        $admin = User::create([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' => '$2y$10$QKiU2NU4zA6gz3l5mbA/uug5sz0KsoFxCiHI65nC6Vhn7bETyoul6',
            'role_id' => $admin_role->id
        ]);


        $super_admin_role = Role::where('name', 'super-admin')->first();

        $super_admin = User::create([
            'name' => 'Super-admin',
            'email' => 'super-admin@gmail.com',
            'password' => '$2y$10$QKiU2NU4zA6gz3l5mbA/uug5sz0KsoFxCiHI65nC6Vhn7bETyoul6',
            'role_id' => $super_admin_role->id
        ]);

        $user_role = Role::where('name', 'user')->first();

        $user = User::create([
            'name' => 'User',
            'email' => 'user@gmail.com',
            'password' => '$2y$10$QKiU2NU4zA6gz3l5mbA/uug5sz0KsoFxCiHI65nC6Vhn7bETyoul6',
            'role_id' => $user_role->id
        ]);
    }
}