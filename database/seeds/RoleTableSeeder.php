<?php

use Illuminate\Database\Seeder;


use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name' => 'super-admin'
            ],
            [
                'name' => 'admin'
            ],
            [
                'name' => 'user'
            ]
        ];

        Role::insert($roles);
    }
}