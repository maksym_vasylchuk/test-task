<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Role;

use Auth;
use Redirect;
use Session;

class DashboardController extends Controller
{

    public function index()
    {
        if (Auth::check()) {
            if (Auth::user()->isAdmin()) {
                return redirect('/admin_panel/dashboard');
            } else {
                return redirect('/admin_panel/login')->with('flash_message_error', 'You do not have permission to view this directory or page');
            }
        } else {
            return redirect('/login');
        }
    }


    public function login(Request $request)
    {

        if (Auth::check() && Auth::user()->isAdmin()) {

            return redirect('/admin_panel/dashboard');
        } else {

            if ($request->isMethod('post')) {

                $data = $request->input();
                if (Auth::attempt(['email' => $data['email'], 'password' => $data['password']])) {
                    if (Auth::user()->isAdmin()) {
                        return redirect()->intended('admin_panel/dashboard');
                    } else {
                        return redirect('/admin_panel/login')->with('flash_message_error', 'You do not have permission to view this directory or page');
                    }
                } else {
                    return redirect('/admin_panel/login')->with('flash_message_error', 'Invalid login or password');
                }
            }
            return view('dashboard.common.login');
        }
    }

    public function dashboard()
    {

        $roles = Role::all();

        return view('dashboard.dashboard', compact('roles'));
    }


    public function logout()
    {
        Session::flush();
        return redirect('/admin_panel/login')->with('flash_message_success', 'Logged out successfully');
    }
}