<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

use App\User;
use DataTables;
use Validator;
use Auth;

class UserController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = User::latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {

                    $btn = '<a href="javascript:void(0)" data-id="' . $row->id . '" class="edit btn btn-primary btn-sm">Edit</a>';
                    $btn .= '<a href="javascript:void(0)" data-id="' . $row->id . '" class="delete btn btn-danger btn-sm">Delete</a>';

                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function store(Request $request)
    {

        $data = [];

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:191',
            'email' => 'required|string|email|max:191|unique:users',
            'password' => 'required|string|min:6|max:191|confirmed',
            'role' => 'nullable|exists:roles,id'
        ]);

        $user = new User([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role_id' => $request->role ? $request->role : null
        ]);

        if ($validator->fails()) {
            $data['errors'] = $validator->errors()->toArray();
        } else {

            $user->save();
            $data['success'] = 'User saved successfully.';
        }

        return response()->json($data);
    }



    public function edit(Request $request)
    {

        if (Auth::id() == $request->user_id) {
            $data['error'] = "Sorry, but you cannot edit yourself.";
            return response()->json($data);
        }

        $user = User::find($request->user_id);

        $data = [];
        if ($user !== null) {
            $data['success'] = $user;
        } else {
            $data['error'] = "Sorry but the user was not found, please try again later!";
        }

        return response()->json($data);
    }



    public function update(Request $request)
    {
        $data = [];
        $user = User::find($request->user_id);

        $rules = [
            'name' => 'required|string|max:191',
            'email' => [
                'required', 'string', 'email', 'max:191',
                Rule::unique('users', 'email')
                    ->whereNot('id', $request->user_id)
            ],

            'role' => 'nullable|exists:roles,id'
        ];

        $newPassword = $request->password;

        if (!empty($newPassword)) {
            $rules['password'] = 'required|string|min:6|max:191|confirmed';
            $user->password = Hash::make($request->input('password'));
        }

        $validator = Validator::make($request->all(), $rules);



        $user->name = $request->name;
        $user->email = $request->email;
        $user->role_id = $request->role ? $request->role : null;



        if ($validator->fails()) {
            $data['errors'] = $validator->errors()->toArray();
        } else {

            $user->save();
            $data['success'] = 'User updated successfully.';
        }

        return response()->json($data);
    }

    public function destroy(Request $request)
    {

        if (Auth::id() == $request->user_id) {
            $data['error'] = "Sorry, but you cannot delete yourself.";
            return response()->json($data);
        }

        $user = User::find($request->user_id);

        $data = [];
        if ($user !== null) {
            $user->delete();
            $data['success'] = "User deleted successfully.";
        } else {
            $data['error'] = "There is no such user";
        }

        return response()->json($data);
    }
}