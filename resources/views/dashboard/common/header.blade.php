<nav class="navbar navbar-expand-lg navbar-light bg-light d-flex justify-content-around">
    <a class="navbar-brand" href="{{route('dashboard')}}">Dashboard</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <ul class="nav navbar-nav navbar-right">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                {{Auth::user()->name}}
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">


                <a class="dropdown-item" href="{{ route('home') }}">
                    Home
                </a>


                <a class="dropdown-item" href="{{ route('admin_panel.logout') }}" onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                    Logout
                </a>



                <form id="logout-form" action="{{ route('admin_panel.logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
        </li>
    </ul>

</nav>