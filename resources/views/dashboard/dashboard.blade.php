@extends('dashboard.layouts.app')

@section('content')

<div class="container">
    <div class="d-flex justify-content-between align-items-center">
        <h1>Users</h1>
        <a class="btn btn-success" href="javascript:void(0)" id="createNewUser"> Create New User</a>
    </div>


    @if(Session::has('flash_message_error'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <p class="text">{{ Session::get('flash_message_error') }}</p><button type="button" class="close"
            data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    @endif

    <div class="message"></div>
    <table class="table table-bordered data-table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Email</th>
                <th>Created at</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>


    <!-- Modal Create/Edit -->
    <div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="userModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="UserModalHeading"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="userForm" name="userForm">
                        <input type="hidden" name="user_id" id="user_id" value="">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" aria-describedby="nameError"
                                placeholder="Enter name" value="" name="name">
                            <small id="nameError" class="form-text text-danger"></small>
                        </div>

                        <div class="form-group">
                            <label for="email">Email address</label>
                            <input type="email" class="form-control" id="email" aria-describedby="emailError"
                                placeholder="Enter email" value="" name="email">
                            <small id="emailError" class="form-text text-danger"></small>
                        </div>

                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" aria-describedby="passwordError"
                                placeholder="Enter password" value="" name="password">
                            <small id="passwordError" class="form-text text-danger"></small>
                        </div>


                        <div class="form-group">
                            <label for="password-confirm">Confirm password</label>
                            <input type="password" class="form-control" id="password-confirm"
                                placeholder="Confirm password" value="" name="password_confirmation">

                        </div>

                        <div class="form-group">
                            <label for="role">User Role</label>
                            <select class="form-control" id="role" name="role" class="role"
                                aria-describedby="roleError">
                                <option value="">Role</option>
                                @foreach ($roles as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                            <small id="roleError" class="form-text text-danger"></small>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="modalBtn" value="">Save changes</button>
                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
    $(function () {
       

    var errorMessage = function(text) {
        return '<div class="alert alert-danger alert-dismissible fade show errorMessage" role="alert"><p class="text">'+text+'</p><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
    }

    var successMessage = function(text) {
        return '<div class="alert alert-success alert-dismissible fade show successMessage" role="alert"><p class="text">'+text+'</p><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
    }


    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });

    function clearError() {
        $('.text-danger').empty();
    }
   

    //Table
      var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('user.index') }}",
        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'email', name: 'email' },
            { data: 'created_at', name: 'created_at' },
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ],
    });

    //Create 
    $('body').on('click', '#createNewUser', function () {
        clearError();
        
        $('#modalBtn').val('saveBtn');
        $('#userForm').trigger("reset");
        $('#user_id').val();
        $('#UserModalHeading').html("Create User");
        $('#userModal').modal('show');
    });


    //Edit
    $('body').on('click', '.edit', function () {
        clearError();
        $('#modalBtn').val('updateBtn');
        var user_id = $(this).data('id');
        $.ajax({
                method: "POST",
                url: "{{ route('user.edit') }}",
                data: {
                    user_id:  user_id,
                    _token: "{{ Session::token() }}"
                },
                success: function(response) {

                    if(response['success']) {
                    $('#UserModalHeading').html("Edit User");
                    $('#userModal').modal('show');
                    $('#user_id').val(response['success'].id);
                    $('#name').val(response['success'].name);
                    $('#email').val(response['success'].email);
                    if(response['success'].role_id) {
                        $("#role option[value=" + response['success'].role_id + "]").attr('selected', 'true');
                    } else {
                        $("#role option[value='']").attr('selected', 'true')
                    }
                    } else if(response['error']){ 
                       error =  errorMessage(response['error']);
                       table.draw();
                       $('.message').html(error);
                        
                    }

                },
                error: function(xhr, ajaxOptions, thrownError) {
                    console.log(xhr);
                    console.log(thrownError);
                    console.log(ajaxOptions);
                }
        });

   });
    

//Store
$('body').on('click','button[value="saveBtn"]', function (e) {

    clearError();
        e.preventDefault();
        $(this).html('Sending..');
    
        $.ajax({
          dataType: 'json',
          method: "POST",
          url: "{{ route('user.save') }}",
          data: $('#userForm').serialize(),
          success: function (response) {
           
     if(response['success']) {
        message =  successMessage(response['success']);
        $('.message').html(message);
                $('#userForm').trigger("reset");
              $('#userModal').modal('hide');
              table.draw();
              
     } else if(response['errors']) {
         
         $.each( response['errors'], function( index, value ){
    
    $('#' + index + 'Error').html(value);
});
     }
     $('button[value="saveBtn"]').html('Save Changes');
              
         
          },
          error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr);
            console.log(thrownError);
            console.log(ajaxOptions);
          }
      });
    });

//Update 
$('body').on('click','button[value="updateBtn"]', function (e) {
    clearError();
        e.preventDefault();
        $(this).html('Sending..');
    
        $.ajax({
          dataType: 'json',
          method: "POST",
          url: "{{ route('user.update') }}",
          data: $('#userForm').serialize(),
          success: function (response) {
     
     if(response['success']) {
        message =  successMessage(response['success']);
        $('.message').html(message);
                $('#userForm').trigger("reset");
              $('#userModal').modal('hide');
              table.draw();
              
     } else if(response['errors']) {
         
         $.each( response['errors'], function( index, value ){
    
    $('#' + index + 'Error').html(value);
});
     }
              
     $('button[value="saveBtn"]').html('Save Changes');
         
          },
          error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr);
            console.log(thrownError);
            console.log(ajaxOptions);
          }
      });
    });


    //Delete
    $('body').on('click', '.delete', function () {
        
        var r = confirm("Are You sure want to delete?");
            if (r == true) {
        var user_id = $(this).data('id');
        $.ajax({
                method: "POST",
                url: "{{ route('user.delete') }}",
                data: {
                    user_id:  user_id,
                },
                success: function(response) {
                

                    if(response['success']) {
                        table.draw();
                        message =  successMessage(response['success']);
                        
                    } else if(response['error']){ 
                        message = errorMessage(response['error']);
                    }
                    $('.message').html(message);
                    table.draw();
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    console.log(xhr);
                    console.log(thrownError);
                    console.log(ajaxOptions);
                }
        });
            }

   });


    });
</script>

@endsection